var Bar = React.createClass({
  getInitialState: function () {
    return {showTooltip: false};
  },
  showTooltip: function(e) {
    this.setState({showTooltip: true});
  },
  hideTooltip: function (e) {
    this.setState({showTooltip: false});
  },
  renderTooltip: function () {
    if(this.state.showTooltip) {
      return <span>Hello {this.props.value}</span>
    } else {
      return <div/>;
    }
  },
  render: function() {
    let {width, value, position, height, chartHeight, padding, color, hoverTooltip} = this.props;

    return (
      <g>
        {/* Draw the colored bar */}
        <rect
          x={position * (width + padding)}
          y={chartHeight - height}
          width={width}
          height={height}
          style={{fill: color}}>
        </rect>

        {/* Draw a transparent full-height overlay to act as a hover target*/}
        <rect
          x={position * (width + padding)}
          y={0}
          width={width}
          height={chartHeight}
          style={{fill: 'transparent'}}
          onMouseEnter={this.showTooltip}
          onMouseLeave={this.hideTooltip}>
        </rect>
      </g>
    );
  }
});

var SelectionHandle = React.createClass({
  getInitialState: function () {
    return {dragging: false};
  },
  onMouseDown: function () {
    this.setState({dragging: true});
  },
  onMouseUp: function () {
    this.setState({dragging: false});
  },
  onMouseMove: function (e) {
    if(this.state.dragging) {
      console.log(e.screenX, e.screenY);
    }
  },
  render: function () {
    return (
      <g>
        <rect x={0} y={0} width={2} height={'100%'} style={{fill: '#555'}}/>
        <rect x={-5} y={'50%'} width={10} height={20} style={{fill: 'red'}}
          onMouseDown={this.onMouseDown} onMouseUp={this.onMouseUp} onMouseMove={this.onMouseMove}/>
      </g>
    );
  }
});

var Histogram = React.createClass({
  onBarHovered: function(e) {
    console.log('hover', e);
  },
  render: function() {
    let {width, height, data, barPadding = 0.4} = this.props;

    // Size the bars so that the highest is a bit below the top of the chart
    let topPadding = 0.3;
    // let max = data.map(d => d.value).reduce((max, cur) => cur > max ? cur : max);
    let max = data.reduce((max, current) => current.value > max.value ? current : max).value;
    let heightScale = (y) => height - (y * height/max) * (1 - topPadding);

    // Set the bar width to fit the bars on the chart
    let numBars = data.length;
    let barWidth = width/(numBars + (numBars - 1)*barPadding);
    let barPaddingPx = barWidth * barPadding;

    return (
      <svg width={width} height={height}>
        {data.map((d, index) => {
          return <Bar
            key={d.id}
            value={d.value}
            color={d.color}
            width={barWidth}
            position={index}
            chartHeight={height}
            height={heightScale(d.value)}
            padding={barPaddingPx}
            onHover={this.onBarHovered}/>
        })}
        <SelectionHandle />
      </svg>
    );
  }
});


var data = Array(24).fill(0).map((val, i) => {
  return {
    id: i,
    value: Math.random(),
    color: Math.random() < 0.3 ? '#E86A6A' : '#86CC6D'
  };
});
ReactDOM.render(
  <Histogram data={data} width={600} height={100}/>,
  document.querySelector('#react-histogram'));
