var d3Chart = {};

d3Chart.computeScales = function (props) {
  var margin = {top: 0, right: 0, bottom: 30, left: 0},
      padding = {top: 0, right: 0, bottom: 0, left: 0},
      outerWidth = props.width,
      outerHeight = props.height,
      innerWidth = outerWidth - margin.left - margin.right,
      innerHeight = outerHeight - margin.top - margin.bottom,
      width = innerWidth - padding.left - padding.right,
      height = innerHeight - padding.top - padding.bottom;

  let [xScale, yScale] = this.generateScales(props.data, width, height);

  return {
    width, height,
    xScale, yScale,
    margin, padding
  };
}

d3Chart.create = function(el, props) {
  this.props = props;

  let {width, height, margin} = this.computeScales(props);

  var svg = d3.select(el)
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  let bars = svg.append('g')
    .classed('bars', true);

  this.update(el, props);
};

function moveToFront() {
  this.parentNode.appendChild(this);
}

d3Chart.update = function(el, props) {
  let {data, onSelection, onZoomIn, fadeInDuration = 0} = props;
  let {width, height, xScale, yScale} = this.computeScales(props);

  let xAxis = d3.svg.axis()
    .scale(xScale)
    .tickValues(xScale.domain().filter((d, i) => !(i % 5)));

  let axisEl = d3.select(el).selectAll('.x.axis')
  axisEl.data([null])
    .enter()
      .append('g')
      .attr('class', 'x axis')
      .attr('transform', `translate(0, ${height + 1})`)
      .attr('fill-opacity', 0)
      .call(xAxis)
      .transition()
      .duration(400)
      .attr('fill-opacity', 1);

  axisEl.call(xAxis);

  var g = d3.select(el).selectAll('.bars');

  var bar = g.selectAll('.bar')
    .data(data, d => d.id);

  g.on('dblclick', function() {
    let index = xScale.indexAtOffset(d3.mouse(this)[0]);
    g.selectAll('.bar')
      .filter((d, i) => i === index)
      // Blink twice
      .transition().duration(75)
      .attr('fill-opacity', 0.1)
      .transition().duration(75)
      .attr('fill-opacity', 1.0)
      .transition().duration(75)
      .attr('fill-opacity', 0.1)
      .transition().duration(75)
      .attr('fill-opacity', 1.0)
      // Then expand to fill the graph, covering the other bars
      .transition()
      .duration(400)
      .each(moveToFront)
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('fill', '#fff')
      // Then remove every element, and notify the parent that we zoomed
      .each("end", function() {
        g.selectAll('.bar').remove();
        g.selectAll('rect').remove();
        onZoomIn(index);
      })
  });

  // Add one bar per data item
  bar.enter()
    .append('rect')
    .classed('bar', true)
    .attr('fill-opacity', 0)
    .transition()
    .duration(fadeInDuration)
    .attr('fill-opacity', 1)

  // Scale and style the bars
  bar
    .attr('x',      d => xScale(d.id))
    .attr('y',      d => height - yScale(d.value))
    .attr('width',  d => xScale.rangeBand())
    .attr('height', d => yScale(d.value))
    .attr('fill',   d => d.color);

  bar.exit().remove();

  this.setupSelectionHandling(g, xScale, onSelection);
};

function initSelectBox(selection, className, fillColor = '#555') {
  return selection.data([null]).enter()
    .append('rect')
    .classed(className, true)
    .attr('width', 0)
    .attr('height', '100%')
    .attr('fill', fillColor)
    .attr('fill-opacity', '0.5')
}

d3Chart.setupSelectionHandling = function (g, xScale, onSelection) {
  // Add some initially-hidden boxes for showing selections
  g.selectAll('.backdrop').data([null]).enter()
    .append('rect')
    .classed('backdrop', true)
    .attr('fill', 'transparent')
    .attr('width', '100%')
    .attr('height', '100%');

  g.selectAll('.leftOfSelection')
    .call(initSelectBox, 'leftOfSelection');

  g.selectAll('.rightOfSelection')
    .call(initSelectBox, 'rightOfSelection');

  g.selectAll('.selectedRegion')
    .call(initSelectBox, 'selectedRegion', 'lightblue');

  let leftOfSelection = g.selectAll('.leftOfSelection');
  let rightOfSelection = g.selectAll('.rightOfSelection');
  let selectedRegion = g.selectAll('.selectedRegion');

  let dragStart = 0;
  let dragRegion = [0, 0];
  let dragSelection = d3.behavior.drag()
    .on('dragstart', function(d, i) {
      // d3.event.sourceEvent.stopPropagation();

      // Grab the coordinates where the drag started
      let [x, y] = d3.mouse(this);
      dragStart = x;

      // Initialize the selection boundary
      selectedRegion.attr('x', x).attr('width', 0);
    })
    .on('drag', function (d, i) {
      let [x, y] = d3.mouse(this);

      // Draw a selection box based on where dragging began
      if(x < dragStart) {
        selectedRegion.attr('x', x).attr('width', dragStart - x);
        dragRegion = [x < 0 ? 0 : x, dragStart];
      } else {
        selectedRegion.attr('x', dragStart).attr('width', x - dragStart);
        dragRegion = [dragStart, x];
      }
    })
    .on('dragend', function () {
      if(dragRegion[1] - dragRegion[0] <= 0) {
        dragRegion = xScale.rangeExtent();
      }
      leftOfSelection.attr('x', 0).attr('width', dragRegion[0]);
      rightOfSelection.attr('x', dragRegion[1]).attr('width', '100%');
      selectedRegion.attr('width', 0);

      // Notify the parent component that some items were selected
      if(onSelection) {
        let selectedIndices = [];
        let selectedIndexRange = dragRegion.map(xScale.indexAtOffset);
        for(let i = selectedIndexRange[0]; i <= selectedIndexRange[1]; i++) {
          selectedIndices.push(i);
        }
        onSelection(selectedIndices);
      }
    });

  g.call(dragSelection);
};

d3Chart.generateScales = function (data, width, height) {
  var xScale = d3.scale.ordinal()
    .domain(data.map((d, i) => d.id))
    .rangeBands([0, width], 0.2);

  // Convert a pixel offset into the index of a data item. Adapted from:
  // http://stackoverflow.com/questions/20758373/inversion-with-ordinal-scale
  xScale.indexAtOffset = (x) => d3.max([0, d3.bisect(xScale.range(), x) - 1]);

  var yScale = d3.scale.linear()
    .domain([0, d3.max(data, d => d.value)])
    .range([0, height]);

  return [xScale, yScale];
};

d3Chart.destroy = function(el) {
  // Any clean-up would go here
  // in this example there is nothing to do
};

// Chart.js

var Chart = React.createClass({
  propTypes: {
    data: React.PropTypes.array,
    domain: React.PropTypes.object,
    onSelection: React.PropTypes.func,
    onZoomIn: React.PropTypes.func,
    fadeInDuration: React.PropTypes.number
  },

  onSelection: function (selectedIndices) {
    if(this.props.onSelection) {
      this.props.onSelection(selectedIndices);
    }
  },

  onZoomIn: function (index) {
    if(this.props.onZoomIn) {
      this.props.onZoomIn(index);
    }
  },

  componentDidMount: function() {
    var el = ReactDOM.findDOMNode(this);
    d3Chart.create(el, this.getChartProps());
  },

  getChartProps: function () {
    return {
      width: this.props.width,
      height: this.props.height,
      onSelection: this.onSelection,
      onZoomIn: this.onZoomIn,
      fadeInDuration: this.props.fadeInDuration,
      data: this.props.data,
      domain: this.props.domain
    };
  },

  componentDidUpdate: function() {
    var el = ReactDOM.findDOMNode(this);
    d3Chart.update(el, this.getChartProps());
  },

  componentWillUnmount: function() {
    var el = ReactDOM.findDOMNode(this);
    d3Chart.destroy(el);
  },

  render: function() {
    return (
      <svg className="Chart"></svg>
    );
  }
});

function DataRows({items}) {
  return (
    <ul>
      {items.map(function(item) {
        let style = {color: item.color};
        return (<li key={item.id} style={style}>{item.value}</li>);
      })}
    </ul>
  );
}

let success_color = '#86CC6D';
let fail_color = '#E86A6A';
function StatusBadge({status}) {
  let icon, color;

  if(status === "success") {
    icon = "fa-check-circle";
    color = success_color;
  } else {
    icon = "fa-times";
    color = fail_color;
  }

  return (
    <span className={`fa ${icon}`} style={{color}}/>
  );
}

function JobExecutionRow({item}) {
  return (
    <tr className="list-row">
      <td className="list-cell status"><StatusBadge status={item.status}/></td>
      <td className="list-cell">{item.name}</td>
      <td className="list-cell">{item.value}</td>
    </tr>
  );
}

function JobExecutionList({items}) {
  return (
    <table>
      <thead>
        <tr>
          <th className="list-header status"></th>
          <th className="list-header">Job</th>
          <th className="list-header">Files Transferred</th>
        </tr>
      </thead>
      <tbody>
        {items.map(item => <JobExecutionRow key={item.id} item={item}/>)}
      </tbody>
    </table>
  );
}

var DataSelector = React.createClass({
  getInitialState: function () {
    return {selectedIndices: [], data: makeData(60), fadeInDuration: 250};
  },
  onSelection: function (selectedIndices) {
    this.setState({selectedIndices});
  },
  itemsToRender: function (data) {
    if(this.state.selectedIndices.length > 0) {
      return this.state.selectedIndices.map(i => this.state.data[i]);
    } else {
      return this.state.data;
    }
  },
  onZoomIn: function (index) {
    console.log('zoom to', index);
    this.setState({selectedIndices: [], data: makeData(24), fadeInDuration: 500});
  },
  render: function () {
    return (
      <div>
        <Chart width={800} height={80} data={this.state.data} onSelection={this.onSelection} onZoomIn={this.onZoomIn} fadeInDuration={this.state.fadeInDuration}/>
        <JobExecutionList items={this.itemsToRender()}/>
      </div>
    );
  }
});

function makeData(count) {
  function randomName() {
    let names = ["Sample Job", "Nemesis Job", "Payroll Transfer"]
    return names[Math.round(Math.random() * 100) % names.length];
  }

  return Array(count).fill(0).map((val, i) => {
    let status = Math.random() < 0.3 ? "failed" : "success";
    return {
      id: i,
      name: randomName(),
      value: Math.floor(Math.random() * 50),
      color: status === "success" ? success_color : fail_color,
      status: status
    };
  });
}

ReactDOM.render(
  <DataSelector />,
  document.querySelector('#d3-chart')
)
