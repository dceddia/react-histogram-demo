To play around with it:

- Install http-server globally:
  (the server is necessary to avoid CORS errors)
    npm install -g http-server
- Start the server
    http-server -p 3000
- Go to http://localhost:3000 in your browser

